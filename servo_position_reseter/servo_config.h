#ifndef SERVO_CONFIG_H
#define SERVO_CONFIG_H


struct Servo_PWM_Configuration {
  const uint16_t servo_min;
  const uint16_t servo_max;
  const uint16_t us_min;
  const uint16_t us_max;
  const uint8_t pwm_frequency;
};

#endif
