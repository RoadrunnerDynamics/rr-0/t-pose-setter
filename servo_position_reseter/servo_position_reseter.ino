#include <Arduino.h>
#include "servo_config.h"

#include <Wire.h>
#include <Adafruit_PWMServoDriver.h>



void initialize_driver(Adafruit_PWMServoDriver& target_driver, const Servo_PWM_Configuration& servo_config) 
{
  target_driver.begin();
  
  // Use the pwm_frequency from servo_config
  target_driver.setPWMFreq(servo_config.pwm_frequency);
  // Shouldn't need to change
  target_driver.setOscillatorFrequency(27000000);
}

void set_servo_to_angle(Adafruit_PWMServoDriver& target_driver, const Servo_PWM_Configuration& servo_config, uint8_t driver_channel, uint8_t desired_angle, uint8_t after_command_delay = 0) 
{
  uint16_t pulseLength = map(desired_angle, 0, 180, servo_config.servo_min, servo_config.servo_max);
  
  // Set the PWM pulse
  target_driver.setPWM(driver_channel, 0, pulseLength);
  
  if (after_command_delay != 0) 
  {
    delay(after_command_delay);
  }
}


int main(void)
{
  Serial.begin(9600);

 
  uint8_t number_of_channels = 16;
  
  const Servo_PWM_Configuration Futaba_S3305_PWM_Profile = {
    .servo_min = 100, // Minimum pulse length for 0 degrees
    .servo_max = 500, // Maximum pulse length for 180 degrees
    .us_min = 600, // Minimum pulse length in microseconds
    .us_max = 2400, // Maximum pulse length in microseconds
    .pwm_frequency = 50 // PWM frequency for the servo
  };

  Adafruit_PWMServoDriver driver = Adafruit_PWMServoDriver(0x40);

  initialize_driver(driver, Futaba_S3305_PWM_Profile);

  while(true)
  {
    for (uint8_t i = 0; i < (number_of_channels - 1); i++)
    {
      set_servo_to_angle(driver, Futaba_S3305_PWM_Profile, i, 90);
      Serial.print("Publishing to channel ");
      Serial.println(i);
      delay(100);
    }

  }
  
  

  return 0;
}
